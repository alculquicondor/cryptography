#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H
#include <fstream>
#include "algorithm.h"

namespace crypto {

class RandomGenerator
{
private:
    static RandomGenerator *pinstance;
    static const NTL::ZZ p, q, n;
    NTL::ZZ x;
protected:
    RandomGenerator();
public:
    static RandomGenerator &getInstance();
    Zint get_Zint(uint nobits);
    long get_ulong(uint nobits);
    NTL::ZZ get_bnd(NTL::ZZ bnd);
};

}

#endif // RANDOMGENERATOR_H
