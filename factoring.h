#ifndef FACTORING_H
#define FACTORING_H
#include "algorithm.h"
#include "randomgenerator.h"

namespace crypto {
extern Zint fermatFact(const Zint &n);
extern Zint pollardP1(const Zint &n, uint B);
extern Zint pollardRho(const Zint &n, uint c = 1);
extern vector<Zint> factor(Zint n);
extern Zint phi(Zint n);
}

#endif // FACTORING_H
