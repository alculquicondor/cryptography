#ifndef CRYPTO_H
#define CRYPTO_H
#include <NTL/ZZ.h>
#include <string>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>

namespace crypto {
typedef NTL::ZZ Zint;
typedef unsigned short int usint;
typedef unsigned int uint;

using std::string;
using std::vector;
using std::pair;
}

#endif // CRYPTO_H
