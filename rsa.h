#ifndef RSA_H
#define RSA_H
#include "algorithm.h"
#include "primegen.h"
#include <sstream>

namespace crypto {

class RSA
{
private:
    NTL::ZZ n, p, q, e, d, phi;
    inline std::string int_to_str(long a) {
        std::stringstream ss;
        ss << a;
        std::string s;
        ss >> s;
        return s;
    }
    inline long str_to_int(const std::string &s) {
        std::stringstream ss;
        ss << s;
        long a;
        ss >> a;
        return a;
    }
    inline std::string ZZ_to_str(const NTL::ZZ &a) {
        std::stringstream ss;
        ss << a;
        std::string s;
        ss >> s;
        return s;
    }
    inline uint numDigits(long a) {
        uint cnt = 0;
        while (a > 0) {
            a /= 10;
            ++ cnt;
        }
        return cnt;
    }
    inline uint numDigits(NTL::ZZ a) {
        return ZZ_to_str(a).size();
    }
    std::string prepare(const std::string &m, uint n_digits, uint z_digits, int compchar);
public:
    RSA(uint nobits);
    RSA(NTL::ZZ p, NTL::ZZ q, NTL::ZZ e);
    RSA(NTL::ZZ n, NTL::ZZ e);

    std::pair<NTL::ZZ, NTL::ZZ> getPublicKey();
    std::pair<NTL::ZZ, NTL::ZZ> getPQ();

    NTL::ZZ cipher(NTL::ZZ m);
    NTL::ZZ decipher(NTL::ZZ c);

    std::string cipher(const std::string &m);
    std::string cipher(const std::string &m, const std::string &alphabet);
    std::string decipher(std::string &c);
    std::string decipher(const std::string &c, const std::string &alphabet);

};

}

#endif // RSA_H
