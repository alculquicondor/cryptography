#ifndef GCDEVALUATORNTL_H
#define GCDEVALUATORNTL_H

#include <iostream>
#include "algorithm.h"

namespace crypto {

class GcdEvaluatorNtl
{
private:
    static const int base = (1<<16);
    static const uint prime[], noprimes = 54;
    uint inverse[base>>1], A[base>>1];
    static GcdEvaluatorNtl *pinstance;
protected:
    GcdEvaluatorNtl();
    GcdEvaluatorNtl(const GcdEvaluatorNtl &g);
    GcdEvaluatorNtl &operator =(const GcdEvaluatorNtl &g);
public:
    static GcdEvaluatorNtl &getInstance();
    NTL::ZZ operator ()(NTL::ZZ u, NTL::ZZ v);
};

}

#endif // GCDEVALUATORNTL_H
