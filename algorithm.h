#ifndef ALGORITHM_H
#define ALGORITHM_H
#include "crypto.h"

namespace crypto {

template <typename UnsignedIntType>
UnsignedIntType gcd(UnsignedIntType a, UnsignedIntType b) {
    UnsignedIntType zero(0);
    if (a < b)
        swap(a, b);
    while (true) {
        if (b == zero)
            return a;
        a %= b;
        if ((a << 1) > b)
            a = b - a;
        if (a == zero)
            return b;
        b %= a;
        if ((b << 1) > a)
            b = a - b;
    }
}

template <typename IntType>
inline IntType mod(IntType a, const IntType &m) {
    if (a >= m or a <= -m)
        a %= m;
    if (a < 0)
        a += m;
    return a;
}

extern NTL::ZZ gcd(NTL::ZZ a, NTL::ZZ b);

template <typename IntType>
inline IntType modu(IntType a, const IntType &m) {
    if (a >= m)
        a %= m;
    return a;
}

template <typename IntType>
vector<IntType> extendedGcd(IntType a, IntType b) {
    IntType u1 = 1, v1 = 0, u2 = 0, v2 = 1, q, r, u, v;
    while (b) {
        q = a / b;
        r = mod(a, b);
        u = u2; v = v2;
        if ((r<<1) > b) {
            r = b - r;
            u2 = -u1 + (q+1) * u2;
            v2 = -v1 + (q+1) * v2;
        } else {
            u2 = u1 - q * u2;
            v2 = v1 - q * v2;
        }
        u1 = u; v1 = v;
        a = b;
        b = r;
    }
    return {u1, v1, a};
}

extern std::vector<NTL::ZZ> extendedGcd(NTL::ZZ a, NTL::ZZ b);

template <typename IntType>
inline IntType inverseModulo(IntType a, IntType m) {
    std::vector<IntType> tmp = extendedGcd(a, m);
    if (tmp[2] != 1)
        throw 3;
    return mod(tmp[0], m);
}

template <typename IntType>
IntType powMod(IntType a, IntType e, IntType m) {
    IntType ans;
    ans = 1;
    while (e > 0) {
        if ((e & 1) != 0)
            ans = mod(ans*a, m);
        a = mod(a*a, m);
        e >>= 1;
    }
    return ans;
}

extern NTL::ZZ powMod(const NTL::ZZ &a, const NTL::ZZ &e, const NTL::ZZ &m);

extern std::pair<Zint, Zint> modSys(const Zint &a, const Zint &m, const Zint &b, const Zint &n);

extern bool testMiller(const Zint &n, const Zint &n1,
                const Zint &r, uint s, const Zint &a);

extern bool testMiller(const Zint &n, uint k);

extern vector<uint> primeSieve(uint limit);

}

#endif // ALGORITHM_H
