#ifndef PRIMEGEN_H
#define PRIMEGEN_H

#include <fstream>
#include "algorithm.h"
#include "randomgenerator.h"

namespace crypto {

class PrimeGen
{
private:
    static PrimeGen *pinstance;
    static const uint prime[], tsz, nopr;
    uint wheel[5760], acwheel[5760], w_size, t_jump;
protected:
    PrimeGen(const PrimeGen &g);
    PrimeGen &operator =(const PrimeGen &g);
    PrimeGen();
    RandomGenerator &randgen;
public:
    static PrimeGen &getInstance();
    NTL::ZZ getPrime(uint bits);
    NTL::ZZ getStrongPrime(uint bits);
    NTL::ZZ getStrongPrime2(uint bits);
};

}

#endif // PRIMEGEN_H
