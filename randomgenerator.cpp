#include "randomgenerator.h"

namespace crypto {

const NTL::ZZ RandomGenerator::p =  NTL::to_ZZ("122787682268551690848973556207448241441739509516622097939455904323865545099753981941599163271894281396198173220002220401801898711993119710784312108000232590311079968931620545799754557160292463403014387882476824331694566769535842175953153589443582787762336329974794510613189319024118837621829902059006992667");
const NTL::ZZ RandomGenerator::q = NTL::to_ZZ("157772376868225325957590891781886213158838623588166962902596585450648981233857228772810590830277708493125643914031800552936436383397822619695032357129520352139147182066394595964056534841592681642331448031303624493662991351862439196754777397388797497606920906300002763724194679592198353988871810633527031617860824843");
const NTL::ZZ RandomGenerator::n = p * q;

RandomGenerator::RandomGenerator()
{
    char tmp[2041];
    std::ifstream randomData("/dev/urandom");
    randomData.getline(tmp, 2040);
    randomData.close();
    x = NTL::ZZFromBytes((unsigned char*)tmp, 2040);
    while (gcd(x, n) != 1)
        ++x;
}

RandomGenerator *RandomGenerator::pinstance = nullptr;
RandomGenerator &RandomGenerator::getInstance()
{
    if (pinstance == nullptr)
        pinstance = new RandomGenerator;
    return *pinstance;
}

Zint RandomGenerator::get_Zint(uint nobits)
{
    uint q = (nobits+9) / 10;
    NTL::ZZ output;
    for (uint i = 0; i < q; ++i) {
        x = modu(x*x, n);
        output = (output << 10) + (x & 1023);
    }
    return trunc_ZZ(output, nobits);
}

long RandomGenerator::get_ulong(uint nobits)
{
    uint q = (nobits+9) / 10;
    NTL::ZZ output;
    for (uint i = 0; i < q; ++i) {
        x = modu(x*x, n);
        output = (output << 10) + (x & 1023);
    }
    return trunc_long(output, nobits);
}

NTL::ZZ RandomGenerator::get_bnd(NTL::ZZ bnd)
{
    NTL::ZZ tmp = get_Zint(NTL::NumBits(bnd)+1);
    return modu(tmp, bnd);
}

}
