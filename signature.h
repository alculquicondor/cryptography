#ifndef SIGNATURE_H
#define SIGNATURE_H
#include "factoring.h"
#include "primegen.h"

namespace crypto {

class Signature
{
private:
    Zint g, p, k, a;
    bool validateGenerator(const Zint &g, const std::vector<Zint> &F);
public:
    Signature(uint nobits = 64);
    Signature(const Zint &g, const Zint &p);
    std::pair<Zint, Zint> getPublicKey();
    Zint first();
    void second(const Zint &B);
};

}

#endif // SIGNATURE_H
