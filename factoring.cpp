#include "factoring.h"

namespace crypto {

Zint fermatFact(const Zint &n)
{
    Zint x = NTL::SqrRoot(n), w, y;
    if (x * x == n)
        return x;
    ++ x;
    while (x < n) {
        w = x * x - n;
        y = NTL::SqrRoot(w);
        if (y * y == w)
            return x - y;
        ++ x;
    }
    return NTL::to_ZZ(1);
}

Zint pollardP1(const Zint &n, uint B)
{
    vector<uint> P = primeSieve(B);
    RandomGenerator &randgen = RandomGenerator::getInstance();
    Zint a = randgen.get_bnd(n-3) + 2, d = gcd(a, n);
    if (d >= 2)
        return d;
    Zint m;
    uint l;
    for (uint i = 0; i < P.size() and P[i] <= B; ++i) {
        l = log(B) / log(P[i]);
        m = NTL::power(NTL::to_ZZ(P[i]), l);
        a = powMod(a, m, n);
    }
    a = gcd(a-1, n);
    if (a > 1 and a < n)
        return a;
    return NTL::to_ZZ(1);
}

Zint pollardRho(const Zint &n, uint c)
{
    Zint x = NTL::to_ZZ(2), y = NTL::to_ZZ(2), p = NTL::to_ZZ(1);
    while (p == 1) {
        x = modu(x * x + c, n);
        y = modu(y * y + c, n);
        y = modu(y * y + c, n);
        p = gcd(mod(x-y, n), n);
    }
    return p;
}

vector<Zint> factor(Zint n)
{
    vector<Zint> ans;
    Zint f;
    while (n > 1) {
        bool done = false;
        for (int i = 1; not done and i < 7; i += 2) {
            f = pollardRho(n, i);
            if (f != n)
                done = true;
        }
        ans.push_back(f);
        while (n % f == 0)
            n /= f;
    }
    return ans;
}

Zint phi(Zint n)
{
    Zint ans = NTL::to_ZZ(1);
    Zint f, t;
    while (n > 1) {
        bool done = false;
        for (int i = 1; not done and i < 5; i += 2) {
            f = pollardRho(n, i);
            if (f != n)
                done = true;
        }
        t = NTL::to_ZZ(1);
        n /= f;
        while (n % f == 0) {
            n /= f;
            t *= f;
        }
        ans *= t * (f-1);
    }
    return ans;
}

}
