#include "algorithm.h"

namespace crypto {

NTL::ZZ powMod(const NTL::ZZ &a, const NTL::ZZ &e, const NTL::ZZ &m) {
    NTL::ZZ ans;
    ans = 1;
    size_t nobits = NumBits(e);
    for (int i = nobits-1; i >= 0; --i) {
        ans = modu(ans * ans, m);
        if (bit(e, i))
            ans = modu(ans * a, m);
    }
    return ans;
}

NTL::ZZ gcd(NTL::ZZ a, NTL::ZZ b)
{
    if (a < b)
        swap(a, b);
    while (true) {
        if (IsZero(b))
            return a;
        a %= b;
        if ((a << 1) > b)
            a = b - a;
        if (IsZero(a))
            return b;
        b %= a;
        if ((b << 1) > a)
            b = a - b;
    }
}

std::vector<NTL::ZZ> extendedGcd(NTL::ZZ a, NTL::ZZ b)
{
    NTL::ZZ u1, v1, u2, v2, q, r, u, v;
    u1 = 1; v1 = 0; u2 = 0; v2 = 1;
    while (not IsZero(b)) {
        q = a / b;
        r = mod(a, b);
        u = u2; v = v2;
        if ((r<<1) > b) {
            r = b - r;
            u2 = -u1 + (q+1) * u2;
            v2 = -v1 + (q+1) * v2;
        } else {
            u2 = u1 - q * u2;
            v2 = v1 - q * v2;
        }
        u1 = u; v1 = v;
        a = b;
        b = r;
    }
    return {u1, v1, a};
}

std::pair<Zint, Zint> modSys(const Zint &a, const Zint &m, const Zint &b, const Zint &n)
{
    auto L = extendedGcd(m, n);
    if (L[2] != 1)
        throw 6;
    Zint nm = n * m;
    Zint rpta = modu(a*mod(L[1], m)*n +
            b*mod(L[0], n)*m, nm);
    return {rpta, nm};
}

bool testMiller(const Zint &n, const Zint &n1,
                const Zint &r, uint s, const Zint &a)
{
    Zint y = powMod(a, r, n);
    if (y != 1 and y != n1) {
        for (uint j = 1; j < s and y != n1; ++j) {
            y = mod(y*y, n);
            if (y == 1)
                return false;
        }
        if (y != n1)
            return false;
    }
    return true;
}

bool testMiller(const Zint &n, uint k)
{
    Zint n1 = n-1, r = n1, a, n3 = n-3;
    uint s = NumTwos(n1);
    MakeOdd(r);
    bool prime = true;
    for (uint i = 0; prime and i < k; ++i)
        prime = testMiller(n, n1, r, s, 2 + NTL::RandomBnd(n3));
    return prime;
}

vector<uint> primeSieve(uint limit)
{
    vector<uint> P = {2, 3, 5};
    P.reserve(limit / log(limit));
    std::bitset<60*1024*1024> isp;
    isp.set();
    uint wheel[] = {4, 2, 4, 2, 4, 6, 2, 6}, wpos = 0;
    uint i, j;
    for (i = 7; (j = i * i) < limit; i += wheel[wpos++]) {
        if (wpos == 8)
            wpos = 0;
        if (isp[i>>1])
            P.push_back(i);
        for ( ; j < limit; j += (i<<1))
            isp.reset(j>>1);
    }
    for ( ; i < limit; i += wheel[wpos++]) {
        if (wpos == 8)
            wpos = 0;
        if (isp[i>>1])
            P.push_back(i);
    }
    return P;
}

}
