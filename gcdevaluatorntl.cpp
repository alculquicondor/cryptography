#include "gcdevaluatorntl.h"

namespace crypto {

const uint GcdEvaluatorNtl::prime[] = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
                                59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109,
                                113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
                                179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233,
                                239, 241, 251, 257};

GcdEvaluatorNtl::GcdEvaluatorNtl()
{
    for (int i = 1; i < (int)base; i += 2)
        inverse[i>>1] = inverseModulo(i, base);
    const int limit = sqrt(base) - 1;
    int x1, step;
    for (int a = 1; a <= limit; ++a) {
        std::vector<int> tmp = extendedGcd(a, base);
        step = base / tmp[2];
        for (int b = -limit; b <= limit; ++b)
            if (b and b % tmp[2] == 0) {
                x1 = tmp[0] * (-b/tmp[2]);
                if (x1 >= base or x1 <= -base)
                    x1 %= base;
                if (x1 < 0)
                    x1 += base;
                if (x1 & 1) {
                    for (int x = x1; x >= 0; x-= step)
                        A[x>>1] = a;
                    for (int x = x1 + step; x < base; x += step)
                        A[x>>1] = a;
                }
            }
    }
}

GcdEvaluatorNtl::GcdEvaluatorNtl(const GcdEvaluatorNtl &g)
{
    for (int i = 0; i < (base>>1); i++) {
        A[i] = g.A[i];
        inverse[i] = g.inverse[i];
    }
}

GcdEvaluatorNtl &GcdEvaluatorNtl::operator =(const GcdEvaluatorNtl &g)
{
    for (int i = 0; i < (base>>1); i++) {
        A[i] = g.A[i];
        inverse[i] = g.inverse[i];
    }
    return *this;
}

GcdEvaluatorNtl *GcdEvaluatorNtl::pinstance = 0;
GcdEvaluatorNtl &GcdEvaluatorNtl::getInstance() {
    if (pinstance == 0)
        pinstance = new GcdEvaluatorNtl;
    return *pinstance;
}

NTL::ZZ GcdEvaluatorNtl::operator ()(NTL::ZZ u, NTL::ZZ v)
{
    uint pow2 = std::min(NumTwos(u), NumTwos(v));
    u >>= pow2;
    v >>= pow2;
    NTL::ZZ ans, tmp;
    ans = 1;
    for (uint i = 0; i < noprimes; ++i)
        while ((not IsZero(u) or not IsZero(v)) and
               u % prime[i] == 0 and v % prime[i] == 0) {
            u /= prime[i];
            v /= prime[i];
            ans *= prime[i];
        }
    usint x;
    int a, b;
    while (not IsZero(u) and not IsZero(v)) {
        if (not IsOdd(u)) {
            MakeOdd(u);
        } else if (not IsOdd(v)) {
            MakeOdd(v);
        } else {
            x = ((u % base) * inverse[(v % base)>>1]);
            a = A[x>>1], b = (-a * x) % base;
            if (abs(b) > sqrt(base))
                b += base;
            tmp = abs(u * a + v * b);
            if (not IsZero(tmp))
                MakeOdd(tmp);
            if (u < v)
                v = tmp;
            else
                u = tmp;
        }
    }
    tmp = IsZero(u) ? v : u;
    for (uint i = 0; i < noprimes; ++i)
        while (tmp % prime[i] == 0)
            tmp /= prime[i];
    return (tmp*ans) << pow2;
}

}
