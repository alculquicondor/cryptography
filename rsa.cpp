#include "rsa.h"

namespace crypto {

RSA::RSA(uint nobits)
{
    PrimeGen &pg = PrimeGen::getInstance();
    RandomGenerator &randomgen = RandomGenerator::getInstance();
    p = pg.getPrime(nobits/2-5);
    q = pg.getPrime(nobits/2+5);
    n = p * q;
    phi = (p-1) * (q-1);
    do {
       e = 1 + randomgen.get_bnd(phi-1);
    } while (gcd(e, phi) != 1);
    d = inverseModulo(e, phi);
}

RSA::RSA(NTL::ZZ p, NTL::ZZ q, NTL::ZZ e) : p(p), q(q), e(e)
{
    n = p * q;
    phi = (p-1) * (q-1);
    if (IsZero(e)) {
        RandomGenerator &randomgen = RandomGenerator::getInstance();
        do {
            e = 1 + randomgen.get_bnd(phi-1);
        } while (gcd(e, phi) != 1);
        this->e = e;
    }
    d = inverseModulo(e, phi);
}

RSA::RSA(NTL::ZZ n, NTL::ZZ e) : n(n), e(e)
{
    p = q = d = phi = 0;
}

std::pair<NTL::ZZ, NTL::ZZ> RSA::getPublicKey()
{
    return std::make_pair(n, e);
}

std::pair<NTL::ZZ, NTL::ZZ> RSA::getPQ()
{
    return std::make_pair(p, q);
}

NTL::ZZ RSA::cipher(NTL::ZZ m)
{
    return powMod(m, e, n);
}

NTL::ZZ RSA::decipher(NTL::ZZ c)
{
    if (IsZero(d))
        throw 4;
    Zint a = powMod(mod(c, p), d, p);
    Zint b = powMod(mod(c, q), d, q);
    return modSys(a, p, b, q).first;
}

std::string RSA::prepare(const std::string &m, uint n_digits, uint z_digits, int compchar = '\0')
{
    std::string str, tmp;
    uint lzeros;
    str.reserve(m.size()*z_digits);
    tmp.reserve(z_digits);
    for (uint i = 0; i < m.size(); ++i) {
        tmp = int_to_str(m[i]);
        lzeros = z_digits - tmp.size();
        str += std::string(lzeros, '0');
        str += tmp;
    }
    lzeros = (str.size()+n_digits-1) / n_digits * n_digits;
    str.reserve(lzeros);
    lzeros -= str.size();
    if (compchar) {
        tmp = int_to_str(compchar);
        while (lzeros >= tmp.size()) {
            lzeros -= tmp.size();
            str += tmp;
        }
        for (int i = 0; lzeros; ++i, --lzeros)
            str.push_back(tmp[i]);
    } else {
        str += std::string(lzeros, '0');
    }
    return str;
}


std::string RSA::cipher(const std::string &m)
{
    uint n_digits = numDigits(n), lzeros;
    std::string str = prepare(m, n_digits-1, 3), cip, tmp;
    cip.reserve(str.size());
    for (uint i = 0; i < str.size(); i += n_digits-1) {
        tmp = ZZ_to_str(cipher(NTL::to_ZZ(str.substr(i, n_digits-1).c_str())));
        lzeros = n_digits - tmp.size();
        cip += std::string(lzeros, '0');
        cip += tmp;
    }
    return cip;
}

std::string RSA::cipher(const std::string &m, const std::string &alphabet)
{
    uint n_digits = numDigits(n), z_digits = numDigits(alphabet.size()-1), lzeros;
    std::string str = m, cip, tmp;
    for (uint i = 0; i < str.size(); ++i)
        str[i] = alphabet.find(str[i]);
    str = prepare(str, n_digits-1, z_digits, alphabet.size()-1);
    cip.reserve(str.size());
    for (uint i = 0; i < str.size(); i += n_digits-1) {
        tmp = ZZ_to_str(cipher(NTL::to_ZZ(str.substr(i, n_digits-1).c_str())));
        lzeros = n_digits - tmp.size();
        cip += std::string(lzeros, '0');
        cip += tmp;
    }
    return cip;
}

std::string RSA::decipher(std::string &c)
{
    uint n_digits = numDigits(n);
    std::string str, dec, tmp;
    str.reserve(c.size());
    uint lzeros;
    for (uint i = 0; i < c.size(); i += n_digits) {
        tmp = ZZ_to_str(decipher(NTL::to_ZZ(c.substr(i, n_digits).c_str())));
        lzeros = n_digits - 1 - tmp.size();
        str += std::string(lzeros, '0');
        str += tmp;
    }
    dec.reserve((str.size()+2)/3);
    for (uint i = 0; i + 3 <= str.size(); i += 3)
        dec.push_back(str_to_int(str.substr(i, 3)));
    return dec;
}

std::string RSA::decipher(const std::string &c, const std::string &alphabet)
{
    uint n_digits = numDigits(n), z_digits = numDigits(alphabet.size()-1);
    std::string str, dec, tmp;
    str.reserve(c.size());
    uint lzeros;
    for (uint i = 0; i < c.size(); i += n_digits) {
        tmp = ZZ_to_str(decipher(NTL::to_ZZ(c.substr(i, n_digits).c_str())));
        lzeros = n_digits - 1 - tmp.size();
        str += std::string(lzeros, '0');
        str += tmp;
    }
    dec.reserve((str.size()+z_digits-1)/z_digits);
    for (uint i = 0; i + z_digits <= str.size(); i += z_digits)
        dec.push_back(alphabet[str_to_int(str.substr(i, z_digits))]);
    return dec;
}

}
