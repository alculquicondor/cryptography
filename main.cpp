#include <iostream>
#include <NTL/ZZ.h>
#include "algorithm.h"
#include "rsa.h"
#include "signature.h"

using namespace std;
using namespace crypto;
using namespace NTL;

void generate() {
    RSA rsa(2048);
    pair<ZZ, ZZ> key = rsa.getPublicKey();
    cout << key.first << " " << key.second << endl;
    pair<ZZ, ZZ> pq = rsa.getPQ();
    cout << pq.first << " " << pq.second << endl;
}

void encrypt() {
    RSA rsa(to_ZZ("33427812416861633407874244026559876441018164426473448797187431304730917151996561549457139929577320407375554323276740558616882760699204393966647461654468355039193325129470536501099800626530852024845924359496133481016779972205801141233867423531337098071600089241856690946480105333357817002482288673571670611241"),
                        to_ZZ("13300418321130049552360299329758997752784248733452500989770673217340255263920831097371609308515342162034123974388784874131728915093602910011636339666478368261928707934279950245462352928812993320303988301305745781325462810426650230436069796596556586107406575081099960119849727451383807069586056241752594972671"));
    string s, alph("abcdefghijklmnopqrstuvwxyz ");
    getline(cin, s);
    s = rsa.cipher(s, alph);
    cout << s << endl;
}

void decrypt() {
    RSA rsa(to_ZZ("144527026533346779278750751076316993"),
            to_ZZ("229260112084128887088326896940600690999674097"),
            to_ZZ("10205258378511635459244975668694907780378751978770320321758324753096137193568801"));
    string s, alph("abcdefghijklmnopqrsteuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789,.:;/?)({}[]¿¡!1234567890-_%*+");
    getline(cin, s);
    s = rsa.decipher(s, alph);
    cout << s << endl;
}

void factor() {
    Zint n = to_ZZ("8814485639");
    vector<Zint> F = factor(n);
    for (auto &x : F)
        cout << "# " << x << endl;
}

void signature() {
    Signature s(56);
    auto p = s.getPublicKey();
    cout << p.first << " " << p.second << endl;
}

int main()
{
    //generate();
    //encrypt();
    //decrypt();
    //factor();
    signature();
    return 0;
}

