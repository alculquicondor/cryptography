#include "signature.h"

namespace crypto {

bool Signature::validateGenerator(const Zint &g, const std::vector<Zint> &F)
{
    Zint p1 = p-1;
    for (const Zint &q : F)
        if (powMod(g, p1/q, p) == 1)
            return false;
    return true;
}

Signature::Signature(uint nobits)
{
    p = PrimeGen::getInstance().getPrime(nobits);
    RandomGenerator &randgen = RandomGenerator::getInstance();
    a = 2 + randgen.get_bnd(p-3);
    std::vector<Zint> F = factor(p-1);
    do {
        g = 2 + randgen.get_bnd(p-2);
    } while (not validateGenerator(g, F));
}

Signature::Signature(const Zint &g, const Zint &p) :
    g(g), p(p)
{
    a = 2 + RandomGenerator::getInstance().get_bnd(p-3);
}

std::pair<Zint, Zint> Signature::getPublicKey()
{
    return {g, p};
}

Zint Signature::first()
{
    return powMod(g, a, p);
}

void Signature::second(const Zint &B)
{
    k = powMod(B, a, p);
}

}
