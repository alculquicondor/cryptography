#include "primegen.h"

namespace crypto {

const uint PrimeGen::prime[] = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
                                59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109,
                                113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
                                179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233,
                                239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293,
                                307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367,
                                373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433,
                                439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499,
                                503, 509, 521, 523, 541, 547 },
            PrimeGen::tsz = 30050, PrimeGen::nopr = 5;

PrimeGen::PrimeGen() : randgen(RandomGenerator::getInstance())
{
    w_size = 0;
    t_jump = 0;
    std::bitset<(tsz>>1)> iscomp;
    iscomp.reset();
    uint tt_jump = 2;
    for (uint i = 0; i < nopr; ++i) {
        tt_jump *= prime[i];
        for (uint j = prime[i] * prime[i]; j < tsz; j += prime[i] << 1)
            iscomp[j>>1] = true;
    }
    uint jump = 2;
    for (uint i = prime[nopr] + 2; t_jump < tt_jump; i += 2) {
        if (not iscomp[i>>1]) {
            t_jump += jump;
            acwheel[w_size] = t_jump;
            wheel[w_size++] = jump;
            jump = 2;
        } else {
            jump += 2;
        }
    }
}

PrimeGen *PrimeGen::pinstance = 0;
PrimeGen &PrimeGen::getInstance()
{
    if (pinstance == 0)
        pinstance = new PrimeGen;
    return *pinstance;
}

NTL::ZZ PrimeGen::getPrime(uint bits)
{
    NTL::ZZ r = randgen.get_Zint(bits), t;
    uint left = (r - prime[nopr]) % t_jump,
            pos = std::lower_bound(acwheel, acwheel+w_size, left) - acwheel;
    if (left != acwheel[pos])
        r += acwheel[pos] - left;
    ++pos;
    if (pos == w_size)
        pos = 0;
    while (true) {
        if (testMiller(r, 60))
            return r;
        r += wheel[pos++];
        if (pos == w_size)
            pos = 0;
    }
}

NTL::ZZ PrimeGen::getStrongPrime(uint bits)
{
    NTL::ZZ r = randgen.get_Zint(bits), t;
    uint left = (r - prime[nopr]) % t_jump,
            pos = std::lower_bound(acwheel, acwheel+w_size, left) - acwheel;
    if (left != acwheel[pos])
        r += acwheel[pos] - left;
    ++pos;
    if (pos == w_size)
        pos = 0;
    while (true) {
        t = (r-1);
        NTL::MakeOdd(t);
        bool valid = t != 1;
        for (uint i = 0; valid and i < 100; ++i)
            if (t % prime[i] == 0)
                valid = false;
        if (valid and testMiller(r, 60))
            return r;
        r += wheel[pos++];
        if (pos == w_size)
            pos = 0;
    }
}

NTL::ZZ PrimeGen::getStrongPrime2(uint bits)
{
    NTL::ZZ r = randgen.get_Zint(bits), t;
    if (not IsOdd(r))
        ++ r;
    while (true) {
        t = (r-1);
        NTL::MakeOdd(t);
        bool valid = t != 1;
        for (uint i = 0; valid and i < 100; ++i)
            if (t % prime[i] == 0)
                valid = false;
        if (valid and testMiller(r, 60))
            return r;
        r += 2;
    }
}

}
