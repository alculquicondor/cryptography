TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    gcdevaluatorntl.cpp \
    algorithm.cpp \
    rsa.cpp \
    primegen.cpp \
    randomgenerator.cpp \
    factoring.cpp \
    signature.cpp

HEADERS += \
    algorithm.h \
    gcdevaluatorntl.h \
    rsa.h \
    primegen.h \
    randomgenerator.h \
    crypto.h \
    factoring.h \
    signature.h


unix: LIBS += -lntl -lgmp
